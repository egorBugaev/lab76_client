import {CHANGE_FIELD, DO_NOTHING, FETCH_GET_SUCCESS, FETCH_POST_SUCCESS} from "./actionTypes";
import axios from '../axios-api';

export const fetchPostSuccess = data => {
	return {type: FETCH_POST_SUCCESS, data};
};
let messages = [];

export const fetchGetSuccess = data => {
	if(data.length !== 0) {
		for (let i = 0; i < data.length; i++) {
			messages.push(data[i])
		}
		return {type: FETCH_GET_SUCCESS, messages};
	}else return{type: DO_NOTHING}
	
};

export const change = (event) => {
	return {type: CHANGE_FIELD , event}
};

export const send = (data) => {
	return dispatch => {
		return axios.post('/messages', data).then(
			response => dispatch(fetchPostSuccess(response.data))
		);
	};
};
export const get = () => {
	return dispatch => {
		return axios.get('/messages/all',).then(
			response => dispatch(fetchGetSuccess(response.data))
		);
	};
};
export const getNew = (count) => {
		return dispatch => {
		return axios.get(`/messages?count=${count}`, ).then(
			response => dispatch(fetchGetSuccess(response.data))
		);
	};
};
export const getByDate = (date) => {
	return dispatch => {
		return axios.get(`/messages?date=${date}`, ).then(
			response => dispatch(fetchGetSuccess(response.data))
		);
	};
};
