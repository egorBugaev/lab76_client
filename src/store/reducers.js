import {CHANGE_FIELD, DO_NOTHING, FETCH_GET_SUCCESS, FETCH_POST_SUCCESS} from "./actionTypes";

const initialState = {
	message: '',
	author: '',
	counter: 2,
	messages: [],
	error: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_POST_SUCCESS:
			console.log(action.data);
			
			if (action.data.error) {
				alert(action.data.error);
				return {...state, message: action.data.error, author: action.data.error, error: true};
			} else {
				return {...state, messages: action.data, error: false};
			}
		case FETCH_GET_SUCCESS:
				return {...state, messages: action.messages, error: false ,};
		case CHANGE_FIELD:
			action.event.persist();
			return {...state, [action.event.target.name]: action.event.target.value, error: false};
		case DO_NOTHING:
			return {...state};
		default:
			return state;
	}
};

export default reducer;
