import React, {Component} from 'react';
import {connect} from "react-redux";
import './App.css';
import {change, get, getByDate, getNew, send} from "./store/actions";

class App extends Component {
	componentDidMount() {
		this.props.getNew(30).then(this.startInterval());
	}
	
	interval = null;
	
	startInterval = () => {
		this.interval = setInterval(() => {
			console.log(this.props.messages);
			this.props.getByDate(this.props.messages[this.props.messages.length - 1].date);
		}, 3000);
	};
	
	sendMessageHandler = (e) => {
		e.preventDefault();
		clearInterval(this.interval);
		this.props.send({
			author: this.props.author,
			message: this.props.message
		}).then(this.startInterval());
	};
	
	render() {
	
		return (
			<div className="container">
				<ul id="list">
					{this.props.messages.map(message => (
						<li key={message.id}>
							<p>{message.author}</p>
							<p>{message.message}</p>
							<p>{message.date}</p>
						</li>
					))}
				</ul>
				<div className="send-block">
					{/*<button className='get' onClick={() => this.props.getNew(this.props.counter)}>Get last messages*/}
					{/*</button>*/}
					{/*<input type="number" onChange={this.props.change} value={this.props.counter} name='counter'/>*/}
					<form onSubmit={this.sendMessageHandler}>
						<input style={this.props.error ? {backgroundColor: 'red'} : null} type="text"
						       value={this.props.author} onChange={this.props.change} name='author'
						       placeholder="Your name:"/>
						<textarea style={this.props.error ? {backgroundColor: 'red'} : null} value={this.props.message}
						          onChange={this.props.change} name='message'
						          placeholder="Your message:"/>
						<button className="send">Send</button>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		message: state.message,
		author: state.author,
		messages: state.messages,
		counter: state.counter,
		error: state.error
	};
};

const mapDispatchToProps = dispatch => {
	return {
		send: (data) => dispatch(send(data)),
		getAll: () => dispatch(get()),
		change: (event) => dispatch(change(event)),
		getNew: (counter) => dispatch(getNew(counter)),
		getByDate: (date) => dispatch(getByDate(date))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

