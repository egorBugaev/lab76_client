import React, { Component } from 'react';
import './App.css';
import {connect} from "react-redux";
import {change, encode, decode} from "./src/store/actions";

class App extends Component {
 
	render() {
    return (
      <div className="App">
	      <p>to encode</p>
        <textarea name='encoded' value={this.props.encoded} onChange={this.props.change} className='text'/>
	      <p>pass</p>
        <input name='pass' value={this.props.pass} onChange={this.props.change} type='text'/>
        <button onClick={() => this.props.encode({text: this.props.decoded, pass:this.props.pass})}>encode</button>
        <button onClick={() => this.props.decode({text: this.props.encoded, pass:this.props.pass})}>decode</button>
	      <p>to decode</p>
        <textarea name='decoded' value={this.props.decoded} onChange={this.props.change} className='text'/>
      </div>
    );
  }
}

const mapStateToProps = state => {
	return {
		encoded: state.encoded,
    decoded: state.decoded,
    pass: state.pass
	};
};

const mapDispatchToProps = dispatch => {
	return {
		encode: (data) => dispatch(encode(data)),
		decode: (data) => dispatch(decode(data)),
    change: (event)=> dispatch(change(event))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

